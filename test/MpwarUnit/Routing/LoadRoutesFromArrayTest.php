<?php

namespace MpwarUnitTest\Routing;

use Mpwar\Routing\LoadRoutesFromArray;
use Mpwar\Routing\Routes;

final class LoadRoutesFromArrayTest extends \PHPUnit_Framework_TestCase
{
    private $routes;
    private $routesLoaded;

    /**
     * @test
     * @dataProvider routeProvider
     */
    public function shouldReturnRoutes($routes)
    {
        $this->givenAnArrayWithRoutes($routes);
        $this->whenLoadingTheRoutes();
        $this->thenTheResultShouldBeInstanceOfRoutes();
    }

    private function givenAnArrayWithRoutes($routes)
    {
        $this->routes = $routes;
    }

    private function whenLoadingTheRoutes(){
        $this->routesLoaded = $this->getRouteCollection($this->routes);
    }

    private function thenTheResultShouldBeInstanceOfRoutes()
    {
        $this->assertInstanceOf(Routes::class, $this->routesLoaded);
    }

    public function routeProvider()
    {
        return [
            'routes' => [
                ['home' => 'home_page',
                    'students' => 'students_resource',
                    'students_scores' => 'students_scores_collection'],
            ],
        ];
    }

    private function getRouteCollection($routes){
        $loadRoutes = new LoadRoutesFromArray();
        return $loadRoutes($routes);
    }
}

