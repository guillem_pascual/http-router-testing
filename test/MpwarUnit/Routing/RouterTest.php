<?php

namespace MpwarUnit\Routing;

use Mpwar\Routing\LoadRoutesFromArray;
use Mpwar\Routing\Router;
use PHPUnit_Framework_TestCase;

final class RouterTest extends \PHPUnit_Framework_TestCase
{
    const HOME_PATH = '/home';
    const HOME_PAGE = 'home_page';

    const STUDENTS_PATH = '/students/1234';
    const STUDENTS_PAGE = 'students_resource';

    const STUDENTS_SCORE_PATH = '/students_scores/1234';
    const STUDENTS_SCORE_PAGE = 'students_scores_collection';

    private $path;
    private $routes;
    private $result;
    
    /**
     * @test
     * @dataProvider routeProvider
    */
    
    public function shouldReturnHomeResource($routes)
    {
        $this->givenTheRoutesAndHomeUri($routes);
        $this->whenGettingTheRoute();
        $this->thenTheResultShouldBeHomeResource();
    }

    /**
     * @test
     * @dataProvider routeProvider
     */

    public function shouldReturnStudentsResource($routes)
    {
        $this->givenTheRoutesAndStudentsUri($routes);
        $this->whenGettingTheRoute();
        $this->thenTheResultShouldBeStudentsResource();
    }

    /**
     * @test
     * @dataProvider routeProvider
     */

    public function shouldReturnStudentsScoreResource($routes)
    {
        $this->givenTheRoutesAndStudentsScoreUri($routes);
        $this->whenGettingTheRoute();
        $this->thenTheResultShouldBeStudentsScoreResource();
    }

    private function givenTheRoutesAndHomeUri($routes)
    {
        $this->path = self::HOME_PATH;
        $this->routes = $this->getRouteCollection($routes);
    }

    private function givenTheRoutesAndStudentsUri($routes)
    {
        $this->path = self::STUDENTS_PATH;
        $this->routes = $this->getRouteCollection($routes);
    }

    private function givenTheRoutesAndStudentsScoreUri($routes)
    {
        $this->path = self::STUDENTS_SCORE_PATH;
        $this->routes = $this->getRouteCollection($routes);
    }

    private function whenGettingTheRoute()
    {
        $router = new Router($this->routes);
        $this->result = $router->getRoute($this->path);
    }

    private function thenTheResultShouldBeHomeResource()
    {
        $this->assertSame(self::HOME_PAGE, $this->result->getRouteId());
    }

    private function thenTheResultShouldBeStudentsResource()
    {
        $this->assertSame(self::STUDENTS_PAGE, $this->result->getRouteId());
    }

    private function thenTheResultShouldBeStudentsScoreResource()
    {
        $this->assertSame(self::STUDENTS_SCORE_PAGE, $this->result->getRouteId());
    }

    public function routeProvider()
    {
        return [
            'routesOK' => [
                ['home' => 'home_page',
                    'students' => 'students_resource',
                    'students_scores' => 'students_scores_collection'],
            ],
        ];
    }

    private function getRouteCollection($routes)
    {
        $loadRoutes = new LoadRoutesFromArray();
        return $loadRoutes($routes);
    }
}

