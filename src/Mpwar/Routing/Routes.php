<?php

namespace Mpwar\Routing;

class Routes
{
    private $allRoutes;

    public function add(Route $route)
    {
        $this->allRoutes[$route->getPath()] = $route;
    }

    public function getAllRoutes()
    {
        return $this->allRoutes;
    }

    public function getRoute($path)
    {
        return $this->allRoutes[$path];
    }
}
