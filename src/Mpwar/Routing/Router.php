<?php

namespace Mpwar\Routing;

class Router
{
    private $routes;
    public function __construct(Routes $routes)
    {
        $this->routes  = $routes;
    }

    public function getRoute($requestUri)
    {
        $routeParts = explode('/',$requestUri);
        $path = $routeParts[1];

        $selectedRoute = $this->routes->getRoute($path);
        $selectedRoute->setParams($routeParts);
        return $selectedRoute;
    }
}
