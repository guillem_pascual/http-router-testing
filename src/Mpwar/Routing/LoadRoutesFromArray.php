<?php

namespace Mpwar\Routing;

class LoadRoutesFromArray
{
    public function __invoke($allRoutes)
    {
        $routes = new Routes();

        foreach ($allRoutes as $path=>$routeId){
            $routes->add(new Route($path, $routeId));
        }
        return $routes;
    }
}

